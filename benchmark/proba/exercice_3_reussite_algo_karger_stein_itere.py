"""
Exercice 3 question E
--------------------------

Réalisation d'un benchmark d'analyse de probabilité pour l'algorithme Karger-Stein itéré.
Le benchmark est constitué de 3 types de graphs différents dont la coupe minimale est connue.
Ainsi il est possible de voir si le type de graph a un impact sur le taux de réussite de l'algorithme.

Pour chacun des graph on souhaite obtenir une visualisation du taux de réussite de l'algorithme en 
fonction du nombre de sommet de notre graph.

"""

from benchmark_utils import multigraph_proba
from projet.common.graph_generator import graph_biparti_complet, graph_complet, graph_cycle
from projet.exercice_3.algo import algo_karger_stein_itere
from projet.common.models import MatriceAdj
from random import choice
from itertools import product


def cardinal(mat, s, v):
    v = [e for e in v if e not in s]
    su = 0
    for i, j in product(v, s):
        su += mat.get_nb_arcs(i, j)
    return su


def func(graph, want):
    graph_copy = graph.copy()
    s = algo_karger_stein_itere(graph_copy, 3)
    got = cardinal(graph, s, graph.sommets)
    return got == want


def random_func_biparti_complet(taille):
    n = choice(range(1, taille - 1))
    m = taille - n
    graph = graph_biparti_complet(taille, n)
    graph = MatriceAdj(graph)
    return graph, min(n, m)


def random_func_graph_complet(taille):
    graph = graph_complet(taille)
    graph = MatriceAdj(graph)
    return graph, taille - 1


def random_func_cycle(taille):
    graph = graph_cycle(taille)
    graph = MatriceAdj(graph)
    return graph, 2


x = range(5, 50, 5)
epochs = 100
multigraph_proba(
    x,
    func,
    [
        random_func_biparti_complet,
        random_func_cycle,
        random_func_graph_complet,
    ],
    epochs,
)
