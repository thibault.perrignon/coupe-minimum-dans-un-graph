import matplotlib.pyplot as plt
import numpy as np

__all__ = ["benchmark_proba", "multigraph_proba"]


# Random func retourne les valeurs d'entrée de func
# Random func prend en entrée la valeur d'un élément de x (soit la taille d'un graph)
def benchmark_proba(x, func, random_func, epochs, plot=True):
    y = []
    for n in x:
        print(n)
        p = np.zeros(epochs)
        for epoch in range(epochs):
            graph, want = random_func(n)
            p[epoch] = func(graph, want)
        y.append(float(p.sum() / epochs))
    if plot:
        plt.bar(list(map(str, x)), y)
        plt.show()

    return x, y


def multigraph_proba(x, func, random_funcs, epochs):
    if random_funcs is None:
        raise ValueError("multigraph_benchmark prend en argument une liste de fonction de génération random ")

    for k, random_func in enumerate(random_funcs):
        x, y = benchmark_proba(x, func, random_func, epochs, False)
        ax = plt.subplot(1, len(random_funcs), k + 1)
        ax.set_title(random_func.__name__)
        plt.plot(x, y)
    plt.show()
