"""
Exercice 1 question H
--------------------------

Réalisation d'un benchmark d'analyse temporelle pour la fonction algo_karger (class: DictAjd).
Le benchmark est constitué de 4 types de graphs différents.
Ainsi il est possible de voir si le type de graph a un impact sur l'exécution de l'algorithme

Pour chacun des graph on souhaite obtenir une visualisation de la complexité temporelle de l'algorithme en 
fonction du nombre de sommet de notre graph.

"""


from benchmark_utils import multigraph_benchmark
from projet.common.graph_generator import graph_alea, graph_biparti_complet, graph_complet, graph_cycle, convert_graph_mat_dict
from projet.common.models import DictAjd
from projet.exercice_1.algo import algo_karger

# Fonction du benchmark
def func(graph: DictAjd, *_):
    algo_karger(graph)


# Fonction de random des valeurs pour la fonction du benchmark
# Le return de cette fonction doit être similaire aux arguments de la fonction benchmark
def random_func_graph_alea(n: int, **_):
    graph = graph_alea(n)
    graph = convert_graph_mat_dict(graph)
    graph_obj = DictAjd(graph)
    return graph_obj


def random_func_graph_biparti_complet(n: int, **_):
    graph = graph_biparti_complet(n)
    graph = convert_graph_mat_dict(graph)
    graph_obj = DictAjd(graph)
    return graph_obj


def random_func_graph_complet(n: int, **_):
    graph = graph_complet(n)
    graph = convert_graph_mat_dict(graph)
    graph_obj = DictAjd(graph)
    return graph_obj


def random_func_graph_cycle(n: int, **_):
    graph = graph_cycle(n)
    graph = convert_graph_mat_dict(graph)
    graph_obj = DictAjd(graph)
    return graph_obj


def main():
    x = list(range(5, 105, 5))
    epochs = 20
    random_funcs = [random_func_graph_biparti_complet, random_func_graph_complet, random_func_graph_cycle, random_func_graph_alea]
    multigraph_benchmark(x, func, epochs, random_funcs)


if __name__ == "__main__":
    main()
