"""
Fichier contenant les fonctions utiles aux différents benchmarks temporel

benchmark_function : permet de calculer le temps d'une fonction
graph_benchmark : permt d'obtenir le temps d'exécution d'une fonction en fonction de x dans un graphique
multigraph_benchmark : permet d'obtenir le temps d'exécution d'une fonction en fonction de x pour plusieurs types de données
"""


from time import time
import matplotlib.pyplot as plt
import numpy as np

__all__ = ["benchmark_function", "graph_benchmark", "multigraph_benchmark"]


def timer(f):
    def wrapper(*args, **kwargs):
        t0 = time()
        f(*args, **kwargs)
        t1 = time()
        return t1 - t0

    return wrapper


@timer
def benchmark_function(func, *args):
    func(*args)


def graph_benchmark(x: list[int | float], func, epochs: int, random_func, plot=True) -> tuple[list, list]:
    y = []

    for val in x:
        t = 0
        print(val)
        for _ in range(epochs):
            data = random_func(n=val, epochs=epochs)
            if isinstance(data, tuple):
                data = list(data)
                t += benchmark_function(func, *data)
            else:
                t += benchmark_function(func, data)
        y.append(t)
    if plot:
        plt.plot(x, y)
        plt.show()

    return x, y


def multigraph_benchmark(x: list[int | float], func, epochs, random_funcs: list = None):
    # Si random_funcs n'est pas une liste de fonctions
    if random_funcs is None:
        raise ValueError("multigraph_benchmark prend en argument une liste de fonction de génération random ")

    for k, random_func in enumerate(random_funcs):
        x, y = graph_benchmark(x, func, epochs, random_func, False)
        ax = plt.subplot(1, len(random_funcs), k + 1)
        ax.set_title(random_func.__name__)
        plt.plot(x, y)
    plt.show()
