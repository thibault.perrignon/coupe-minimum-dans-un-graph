"""
Exercice 1 question B
--------------------------

Réalisation d'un benchmark d'analyse temporelle pour la méthode de contraction (class: MatriceAdj).
Le benchmark est constitué de 4 types de graphs différents.
Ainsi il est possible de voir si le type de graph a un impact sur l'exécution de la méthode de contraction.

Pour chacun des graph on souhaite obtenir une visualisation de la complexité temporelle de la méthode de contraction en 
fonction du nombre de sommet de notre graph.

"""


from benchmark_utils import multigraph_benchmark
from projet.common.graph_generator import graph_alea, graph_biparti_complet, graph_complet, graph_cycle
from projet.common.models import MatriceAdj
from random import randint

# Fonction du benchmark
def func(graph: MatriceAdj, i: int, j: int, *_):
    for _ in range(100):
        graph_copy = graph.copy()
        graph_copy.contraction(i, j)


# Fonction de random des valeurs pour la fonction du benchmark
# Le return de cette fonction doit être similaire aux arguments de la fonction benchmark
def random_func_graph_alea(n: int, **_):
    graph = graph_alea(n)
    graph_obj = MatriceAdj(graph)
    i = randint(1, n)
    while (j := randint(1, n)) == i:
        pass
    return graph_obj, i, j


def random_func_graph_biparti_complet(n: int, **_):
    graph = graph_biparti_complet(n)
    graph_obj = MatriceAdj(graph)
    i = randint(1, n)
    while (j := randint(1, n)) == i:
        pass
    return graph_obj, i, j


def random_func_graph_complet(n: int, **_):
    graph = graph_complet(n)
    graph_obj = MatriceAdj(graph)
    i = randint(1, n)
    while (j := randint(1, n)) == i:
        pass
    return graph_obj, i, j


def random_func_graph_cycle(n: int, **_):
    graph = graph_cycle(n)
    graph_obj = MatriceAdj(graph)
    i = randint(1, n)
    while (j := randint(1, n)) == i:
        pass
    return graph_obj, i, j


def main():
    x = list(range(5, 105, 5))
    epochs = 20
    random_funcs = [random_func_graph_biparti_complet, random_func_graph_complet, random_func_graph_cycle, random_func_graph_alea]
    multigraph_benchmark(x, func, epochs, random_funcs)


if __name__ == "__main__":
    main()
