"""
Fichier commun pour tout le projet contenant les différentes structures de donénes utilisées
- MATRICE D'ADJACENCE
- DICTIONNAIRE D'ADJACENCE
"""

from random import choices


class MatriceAdj:
    def __init__(self, mat: list[list[int]]):
        self.mat = list(mat)
        self.taille = len(mat)
        for i in range(self.taille):
            self.mat[i] = list(self.mat[i])

    @property
    def sommets(self):
        return [i + 1 for i, e in enumerate(self.mat) if e != [0] * self.taille]

    def __iter__(self):
        for vect in self.mat:
            yield vect

    def __getitem__(self, i: int):
        return self.mat[i]

    def __repr__(self):
        return "\n".join([f"{' '.join(map(str, ligne))}" for ligne in self])

    def __len__(self):
        return len(self.sommets)

    def get_nb_arcs(self, i, j):
        return self[i - 1][j - 1]

    def copy(self):
        return MatriceAdj(self.mat.copy())

    def sum_arete_sommets(self):
        return [sum(ligne) for ligne in self]

    def sum_all(self):
        return sum(self.sum_arete_sommets())

    def tirage_sommet_alea(self):
        sum_aretes = self.sum_arete_sommets()
        sum_all = self.sum_all()
        i = choices(list(range(1, self.taille + 1)), [elem / sum_all for elem in sum_aretes])[0]
        j = choices(list(range(1, len(self[i - 1]) + 1)), [elem / sum(self[i - 1]) for elem in self[i - 1]])[0]
        return i, j

    def contraction(self, i: int, j: int):
        # Mise à jour des valeurs de la matrice (post contraction)
        i -= 1
        j -= 1
        # Fusionner le point i avec le point j
        for k in range(self.taille):
            if not (k == i or k == j):
                self.mat[i][k] += self.mat[j][k]
                self.mat[k][i] += self.mat[k][j]
            self.mat[k][j] = 0
        # Suppression des arrête du sommet j
        self.mat[j] = [0] * self.taille

        # Return de la matrice si nécessaire
        return self.mat


class DictAjd:
    def __init__(self, dico: dict[int, list]):
        self.dico = {i: j for i, j in dico.items() if len(j) > 0}
        self.taille = len(dico)

    @property
    def sommets(self):
        return list(self.dico)

    def __iter__(self):
        for key, value in self.dico.items():
            yield key, value

    def __getitem__(self, key):
        return self.dico[key]

    def __repr__(self):
        return "\n".join([f"{key} : {' '.join(map(str, value))}" for key, value in self])

    def __len__(self):
        return len(self.sommets)

    def get_nb_arcs(self, i, j):
        return self[i].count(j)

    def copy(self):
        new_dico = {}
        for key, value in self:
            new_dico[key] = list(value)
        return DictAjd(new_dico)

    def sum_arete_sommets(self):
        return [len(values) for _, values in self]

    def sum_all(self):
        return sum(self.sum_arete_sommets())

    def tirage_sommet_alea(self):
        sum_aretes = self.sum_arete_sommets()
        sum_all = self.sum_all()
        i = choices(list(self.dico.keys()), [elem / sum_all for elem in sum_aretes])[0]
        j = choices(self[i], [elem / sum(self[i]) for elem in self[i]])[0]
        return i, j

    def contraction(self, i, j):
        # Ajout des valeurs de j dans i
        self.dico[i] = [elem for elem in self.dico[j] + self.dico[i] if elem != i and elem != j]
        # Remplacement de j par i dans tous les k appartenant au nouveau i
        for k in self.dico[i]:
            self.dico[k] = [elem if elem != j else i for elem in self.dico[k]]
        # Suppression de j
        del self.dico[j]

        # Return du dico si nécessaire
        return self.dico
