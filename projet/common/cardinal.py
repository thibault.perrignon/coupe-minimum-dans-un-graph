"""
Fichier commun pour tout le projet permettant de calculer le cardinal d'une graph sachant une coupe
"""
from itertools import product


def cardinal(mat, s, v):
    v = [e for e in v if e not in s]
    su = 0
    for i, j in product(v, s):
        su += mat.get_nb_arcs(i, j)
    return su
