"""
Génération de graphique aléatoire
"""

from random import randint, choices
import numpy as np

__all__ = ["graph_complet", "graph_cycle", "graph_biparti_complet", "graph_biparti_alea", "graph_alea", "convert_graph_mat_dict"]


def correction_one_graph_iter(graph: np.ndarray, i, cache):
    idxs = np.where(graph[i] >= 1)[0]
    for j in idxs:
        if j not in cache:
            cache.append(j)
            correction_one_graph_iter(graph, j, cache)

    return cache


def correction_alea(graph: np.ndarray, biparti_transition: int = 0):
    # Correction des sommets solo:
    # Tous les sommets doivent être relié à au moins un autre sommets
    sum_graph = graph.sum(axis=1)
    idxs = np.where(sum_graph == 0)[0]
    for i in idxs:
        while (j := choices(range(0, len(graph[i])), [1 / len(graph[i]) for _ in graph[i]])[0]) == i:
            pass
        graph[i, j] = 1
        graph[j, i] = 1

    # Correction chemin de sommets:
    # Tous les sommets doivent pouvoir rejoindre le sommet 1 d'une certaine façon
    cache = [0]
    cache = correction_one_graph_iter(graph, 0, cache)
    for sommet in range(0, len(graph)):
        if sommet not in cache:
            if sommet < biparti_transition:
                i = 1
            else:
                i = biparti_transition
            graph[sommet][i] = 1
            graph[i][sommet] = 1

    return graph


def graph_complet(taille, *_):
    mat = np.ones((taille, taille), dtype=int)
    np.fill_diagonal(mat, 0)
    return mat


def graph_cycle(taille, *_):
    mat = np.zeros((taille, taille))
    mini_mat = np.zeros((taille - 1, taille - 1))
    np.fill_diagonal(mini_mat, 1)
    mat[1:taille, : taille - 1] = mini_mat
    mat[taille - 1, 0] = 1
    mat = np.tril(mat) + np.tril(mat, -1).T
    return mat


def graph_biparti_complet(taille, n=None, *_):
    return graph_biparti_alea(taille, p=1, n=n)


def graph_biparti_alea(taille, p=0.5, n=None):
    if n is None:
        n = randint(1, taille - 1)
    m = taille - n
    choices = np.random.choice(a=2, size=(m, n), p=[1 - p, p])
    mat = np.zeros((taille, taille))
    mat[n:, :n] = choices
    mat = np.tril(mat) + np.tril(mat, -1).T
    mat = correction_alea(mat, n)
    return mat


def graph_alea(taille, p=0.5):
    mat = np.random.choice(a=2, size=(taille, taille), p=[1 - p, p])
    mat = np.tril(mat) + np.tril(mat, -1).T
    mat.dtype = int
    np.fill_diagonal(mat, 0)
    mat = correction_alea(mat)
    return mat


def convert_graph_mat_dict(mat):
    dic = {}
    for i in range(1, len(mat) + 1):
        dic[i] = []
        for j in range(1, len(mat) + 1):
            dic[i] += [j] * int(mat[i - 1][j - 1])
    return dic
