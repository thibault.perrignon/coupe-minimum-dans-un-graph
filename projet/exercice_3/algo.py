"""
Fichier regroupant les fonctions nécessaire à l'algorithme de Karger de l'exercice 3
"""

from projet.common.models import MatriceAdj, DictAjd
from math import ceil, sqrt, inf
from itertools import product, combinations, chain
from projet.common.cardinal import cardinal


def coupe_min(mat: MatriceAdj | DictAjd):
    v = mat.sommets
    cmin = inf
    smin = None
    for s in chain.from_iterable(combinations(v, r) for r in range(1, len(mat))):
        if (c := cardinal(mat, s, v)) < cmin:
            cmin = c
            smin = s
    return smin


def contraction_partielle(mat: MatriceAdj | DictAjd, t: int) -> MatriceAdj | DictAjd:
    g = mat.copy()
    while len(g) > t:
        i, j = g.tirage_sommet_alea()
        g.contraction(i, j)
    return g


def algo_karger_stein(mat: MatriceAdj | DictAjd):
    if len(mat) <= 6:
        return coupe_min(mat)

    t = ceil(1 + len(mat) / sqrt(2))
    v = mat.sommets

    g1 = contraction_partielle(mat, t)
    s1 = algo_karger_stein(g1)
    m1 = cardinal(mat, s1, v)

    g2 = contraction_partielle(mat, t)
    s2 = algo_karger_stein(g2)
    m2 = cardinal(mat, s2, v)

    if m1 < m2:
        return s1
    return s2


def algo_karger_stein_itere(mat: MatriceAdj | DictAjd, T: int):
    m_opti = inf
    s_opti = []
    for _ in range(T):
        mat_copy = mat.copy()
        s = algo_karger_stein(mat_copy)
        m = cardinal(mat, s, mat.sommets)
        if m < m_opti:
            m_opti = m
            s_opti = s
    return s_opti
