"""
Fichier regroupant les fonctions nécessaire à l'algorithme de Karger de l'exercice 2
"""

from ..common.models import MatriceAdj, DictAjd
from ..common.cardinal import cardinal
from math import inf


def algo_karger(mat: MatriceAdj | DictAjd):
    d = {i: [i] for i in range(1, len(mat) + 1)}
    while len(mat) > 2:
        i, j = mat.tirage_sommet_alea()
        d[i] += d[j]
        del d[j]
        mat.contraction(i, j)
    return list(d.values())[0]


def algo_karger_itere(mat: MatriceAdj | DictAjd, T: int):
    m_opti = inf
    s_opti = []
    for _ in range(T):
        mat_copy = mat.copy()
        s = algo_karger(mat_copy)
        m = cardinal(mat, s, mat.sommets)
        if m < m_opti:
            m_opti = m
            s_opti = s
    return s_opti
