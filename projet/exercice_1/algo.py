"""
Fichier regroupant les fonctions nécessaire à l'algorithme de Karger de l'exercice 1
"""

from ..common.models import MatriceAdj, DictAjd


def algo_karger(mat: MatriceAdj | DictAjd):
    d = {i: [i] for i in range(1, len(mat) + 1)}
    while len(mat) > 2:
        i, j = mat.tirage_sommet_alea()
        d[i] += d[j]
        del d[j]
        mat.contraction(i, j)
    return list(d.values())[0]
