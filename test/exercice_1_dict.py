"""
Fichier contenant les tests pour la structure de données Dictionnaire d'adjacences
"""

from projet.common.models import DictAjd
from test_printer import print_test


dico = {1: [2, 5], 2: [1, 3, 5], 3: [2, 4], 4: [3, 5], 5: [1, 2, 4]}

d = DictAjd(dico)


def main():
    print("Nouvel object DictAjd : \n", d)

    print("Test __iter__")
    for key, value in d:
        print(key, value)

    print_test(d.sum_arete_sommets)
    print_test(d.sum_all)
    print_test(d.tirage_sommet_alea)
    print_test(d.contraction, 1, 2, pre_print=f"{d.dico}")


if __name__ == "__main__":
    main()
