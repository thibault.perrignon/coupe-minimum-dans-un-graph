"""
Fichier contenant le test de l'algo de karger de l'exercice 1
"""

from projet.exercice_1.algo import algo_karger
from exercice_1_dict import d
from exercice_1_matrice import mat
from test_printer import print_test


def main():
    print_test(algo_karger, d, pre_print=f"Algo Karger sur une structure DictAdj\n{d}\n")
    print_test(algo_karger, mat, pre_print=f"Algo Karger sur une structure MatriceAdj\n{mat}\n")


if __name__ == "__main__":
    main()
