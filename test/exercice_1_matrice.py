"""
Fichier contenant les tests pour la structure de données Matrice d'adjacences
"""
from projet.common.models import MatriceAdj
from projet.exercice_1.algo import algo_karger
from test_printer import print_test

mat = MatriceAdj([[0, 1, 0, 0, 1], [1, 0, 1, 0, 1], [0, 1, 0, 1, 0], [0, 0, 1, 0, 1], [1, 1, 0, 1, 0]])


def main():
    print("Nouvel object MatriceAdj : \n", mat)

    print("Test __iter__")
    for value in mat:
        print(value)

    print_test(mat.sum_arete_sommets)
    print_test(mat.sum_all)
    print_test(mat.tirage_sommet_alea)
    print_test(mat.contraction, 2, 3, pre_print=f"Matrice avant contraction = {mat.mat}")


if __name__ == "__main__":
    main()
