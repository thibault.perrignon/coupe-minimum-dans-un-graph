"""
Fichier contenant les tests pour l'algo karger de l'exercice 2
"""

from projet.exercice_2.algo import algo_karger_itere
from projet.common.models import MatriceAdj, DictAjd
from projet.common.graph_generator import graph_complet, convert_graph_mat_dict
from test_printer import print_test


def main():
    graph = graph_complet(10)
    print(graph)

    # Tests
    mat = MatriceAdj(graph)
    dico = DictAjd(convert_graph_mat_dict(graph))
    T = 5
    print_test(algo_karger_itere, mat, T)
    print_test(algo_karger_itere, dico, T)


if __name__ == "__main__":
    main()
