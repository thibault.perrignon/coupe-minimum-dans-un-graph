"""
Fichier contenant les fonctions utiles pour les tests
"""


def print_test(func, *args, pre_print: str = ""):
    print(f"Test - pre-print = {pre_print} - {func.__qualname__} = {func(*args)}")
