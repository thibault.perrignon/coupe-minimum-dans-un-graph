"""
Fichier contenant les tests pour la génération des graphs
"""

from projet.common.graph_generator import (
    graph_alea,
    graph_biparti_alea,
    graph_biparti_complet,
    graph_complet,
    graph_cycle,
    convert_graph_mat_dict,
)
import numpy as np

# Taille des graphs
taille = 5


# Test graph complet
print(f"Test graph complet {taille=}")
print(graph_complet(taille))

# Test graph aleatoire
print(f"Test graph aléatoire {taille=} ; p=0.5")
print(graph_alea(taille))
print(f"Test graph aléatoire {taille=} ; p=1")
print(graph_alea(taille, p=1))

# Test graph cycle
print(f"Test graph cycle {taille=}")
print(graph_cycle(taille))

# Test graph biparti aléatoire
print(f"Test graph cycle {taille=} ; p=0.5")
print(np.array(graph_biparti_alea(taille, p=0.5)))
print(f"Test graph cycle {taille=} ; p=1")
print(np.array(graph_biparti_alea(taille, p=1)))

# Test convertion
print(f"Test convert graph")
print(g := graph_alea(taille, p=0.5))
print(convert_graph_mat_dict(g))
