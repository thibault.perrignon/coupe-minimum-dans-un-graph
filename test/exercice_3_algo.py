"""
Fichier contenant les test pour l'algo karger_stein_itere
"""

from projet.exercice_3.algo import algo_karger_stein, algo_karger_stein_itere
from projet.common.models import DictAjd, MatriceAdj
from test_printer import print_test
from projet.common.graph_generator import graph_alea, convert_graph_mat_dict

import networkx as nx
import matplotlib.pyplot as plt


def main():
    print_test(algo_karger_stein, d, pre_print=f"Algo Karger sur une structure DictAdj\n{d}\n")
    print_test(
        algo_karger_stein,
        mat,
        pre_print=f"Algo Karger sur une structure MatriceAdj\n{mat}\n",
    )
    print_test(algo_karger_stein_itere, d, 10, pre_print=f"Algo Karger sur une structure DictAdj\n{d}\n")
    print_test(
        algo_karger_stein_itere,
        mat,
        10,
        pre_print=f"Algo Karger sur une structure MatriceAdj\n{mat}\n",
    )


if __name__ == "__main__":

    G = nx.Graph()
    G.add_nodes_from(range(1, 7))

    # g = graph_alea(6,p=0.5)
    g = [
        [0, 1, 1, 0, 0, 0, 0],
        [1, 0, 1, 0, 0, 0, 0],
        [1, 1, 0, 1, 0, 0, 0],
        [0, 0, 1, 0, 1, 1, 0],
        [0, 0, 0, 1, 0, 1, 1],
        [0, 0, 0, 1, 1, 0, 1],
        [0, 0, 0, 0, 1, 1, 0],
    ]
    for i in range(len(g)):
        for j in range(len(g)):
            if g[i][j] > 0:
                G.add_edge(i + 1, j + 1)
    mat = MatriceAdj(g)

    g = convert_graph_mat_dict(g)
    d = DictAjd(g)

    main()

    nx.draw(G, with_labels=True, font_weight="bold")
    plt.show()
