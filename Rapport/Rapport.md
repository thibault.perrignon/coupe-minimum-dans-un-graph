```pdf
{
	"url":"./Page de garde.pdf",
	"scale": 1
}
```

# Introduction

## Problème

En informatique théorique, une coupe minimum d'un graphe est une coupe contenant un nombre minimal d'arêtes. Dans ce projet, nous allons traiter différents algorithmes connues permettant de résoudre le problème d'une coupe minimum dans un graphe. Nous étudierons la complexité temporelle de ces algorithmes ainsi que leur probabilié de réussite.


## Outils techniques

Afin d'effectuer ce projet, nous avons choisi d'utiliser le langage de programmation Python. En effet, ce langage propose plusieurs librairies, simples et rapides à installer, telles que :
- **matplotlib** pour la visualisation de graphique
- **numpy** pour la génération et modification rapide de matrices
- **random** pour générer l'aléatoire

Cependant, Python ne présente pas que des points positifs : sa lenteur est un fort désavantage à son utilisation dans le cas où nous souhaitons analyser un algorithme sur plusieurs itérations. Afin de diminuer au maximum ce désavantage, nous aurions pu choisir d'utiliser la librarie numpy dans nos algorithmes. Cependant, nous ne souhaitions pas que cette librairie ait un impact sur l'analyse nos algorithmes. En effet, à des fins de comparaison, si un algorithme utilise majoritairement numpy et un autre algorithme ne l'utilise pas, on peut s'attendre à ce que la différence de complexité temporelle expérimentale entre les deux algorithmes soit biaisé par l'utilisation de numpy. 

C'est pourquoi nous avons choisi la structure de code suivante :
- Aucun algorithmes ou fonctions utiles aux algorithmes ne devront utiliser de librairies permettant d'améliorer la rapidité d'exécution du programme. Utilisation de python dit "basique"
- Toutes fonctions non utiles aux algorithmes devront utiliser numpy afin d'améliorer la vitesse d'exécution des benchmarks


## Structure du code

Nous avons choisi de séparer notre code en 3 grands dossiers. 

Le dossier `./projet` regroupe l'ensemble des fichiers de fonctions et de classes qui permettent la résolution des algorithmes. On retrouve dans ce dossier 4 sous-dossiers. Les dossier contenant "exercice" dans leur nom permettent de stocker les algorithmes pour un certains exercice. En revanche, le dossier `./projet/common` regroupe les fichiers contenant le code commun à tous les exercices. On retrouvera par exemple:
- les **models** qui correspondent aux structures de données utilisées tout au long du projet
- les générateurs de graphiques utiles aux tests 
- les fonctions utiles à tous les algorithmes
Ainsi, en séparant les fichiers de cette façon, nous évitons la réécriture de code.

Le dossier `./tests` regroupe l'ensemble des tests. Dans ce dossier, on considère qu'un fichier permet de tester une fonctionnalité bien précise. Par exemple, nous avons un fichier permettant de tester les méthodes de chacun de nos modèles de données et un fichier permettant de tester nos générateurs de graphiques. Afin de simplifier l'écriture de ces tests, nous pouvons utiliser la fonction suivante écrite dans le fichier `./tests/test_printer.py`

```python
def print_test(func, *args, pre_print: str = ""):
	print(f"Test - pre-print = {pre_print} - {func.__qualname__} = {func(*args)}")
```

Ainsi il est possible de passer une fonction avec des arguments en entrée afin de la tester.

Enfin, le dossier `./benchmark` permet de regrouper l'ensemble de nos benchmark. Nous appelons benchmark une étude expérimentale permettant de connaître la variation du temps d'exécution ou du taux de réussite de nos algorithmes. C'est pourquoi nous regroupons nos benchmark dans deux sous-dossiers différents :
- `./benchmark/proba` regroupe les fichiers permettant la visualisation du taux de réussite de nos algorithmes
- `./benchmark/temporel` regroupe les fichiers permettant la visualisation du temps d'exécution de nos fonctions

Dans le but de simplifier cette visualisation, nos fichiers de benchmark utilisent la librairie matplotlib.pyplot afin d'afficher des courbes en fonction de la taille de nos graphiques. 

Chacun de nos benchmark utilisent une fonction à tester, une fonction permettant de générer aléatoirement des données de tests et un nombre d'itératons appelé **epochs**. Ce nombre d'itération permet de relancer n fois la même fonction sur des jeux de données aléatoires différents. Cela nous permet d'obtenir une moyenne de temps d'exécution non biaisé par l'entrée de la fonction.

Comme précisé précédemment, notre utilisation de python est lente dû à notre envie de ne pas biaisé nos résultats de tests expérimentaux. Ainsi, l'exécution de nos benchmark prennent un certains temps en fonction du nombre d'itérations de ces fonctions.

<div style="page-break-after: always;"></div>

# Exercice 1

## Structure de type Matrice d'adjacence

Tout au long de cet exercice, nous souhaitons tester un algorithme de Karger simple permettant de résoudre le problème de coupures minimales dans un graphique. Ainsi nous avons l'algorithme suivant : 

![Algo Karger](img/Algo_Karger_1.png)

On sait que dans cet algorithme, la fonction de contraction d'arêtes présente dans la boucle **tant que** prend un temps $\mathcal{O}(n)$. Ainsi, nous voulons vérifier si cette information théorique peut être vérifié par une étude expérimentale. Pour cela nous avons choisi de concevoir un structure de données permettant de fonctionner comme une matrice d'adjacence. 

Pour cela nous avons choisi d'écrire un code permettant de générer un objet de la classe **MatriceAdj** (code visible dans le fichier `./projet/common/models.py`). Cet objet est défini par les attributs :
- sommets : nombre de sommets restant dans le graphique
- mat : matrice d'adjacence
- taille : taille initiale du graphique (sans suppression de sommets)

Ainsi, afin de permettre la contraction d'arêtes sur cet objet, nous avons créé une méthode contraction qui prend en paramètre i, le sommet restant, et j, le sommet à supprimer dans le graphique.  Ainsi nous obtenons le code suivant : 
```python
def contraction(self, i: int, j: int):
	# Mise à jour des valeurs de la matrice (post contraction)
	i -= 1
	j -= 1
	# Fusionner le point i avec le point j
	for k in range(self.taille):
		if not (k == i or k == j):
			self.mat[i][k] += self.mat[j][k]
			self.mat[k][i] += self.mat[k][j]
		self.mat[k][j] = 0
	# Suppression des arrête du sommet j
	self.mat[j] = [0] * self.taille
	# Return de la matrice si nécessaire
	return self.mat
```

N'utilisant pas numpy dans cette fonction, il nous faut parcourir la matrice d'adjacence à l'aide de boucle for.
Il est également important de notifier qu'une matrice d'adjacence est symétrique. Ainsi, lorsque nous changeons une valeur de notre matrice, il faut appliquer la symétrie.  De plus, nous avons choisi de ne pas supprimer les lignes et colonnes des sommets supprimés afin de ne pas avoir à manipuler les indices i et j. En effet, si nous supprimons le sommet 2 d'indice 1 (sommet $\in [\![1,n]\!]$), le sommet 3, initialement d'indice 2, aura 1 comme nouvel indice dans notre matrice. Ainsi, pour éviter ce problème, nous avons choisi de remplir les colonnes et lignes du sommet supprimé par des valeurs 0 : aucun arc ne rejoint ce sommet.

Afin de vérifier le bon fonctionnement de cette méthode, nous avons écrit un test dans le fichier `./test/exercice_1_matrice.py`

Nous souhaitons désormais visualiser la complexité temporelle expérimentale de cette fonction. Ainsi, nous avons écrit un benchmark pour cette méthode et cet objet disponible à partir du fichier `./benchmark/temporel/contraction_matrice.py`. Celui-ci permet de lancer un benchmark de notre méthode de contraction pour la structure de donnée d'une matrice d'adjacence constitué des itérations suivantes : 
- La fonction de benchmark (celle qui obtiendra un temps en sortie) est constitué de 100 itérations de la fonction de contraction d'arêtes sur les mêmes valeurs du graphique, de i et de j
- Pour chaque taille n, on répètera la fonction de benchmark 20 fois (valeur de epochs)
- On fera varier la taille de n de 5 à 100 avec un pas de 5

L'importance d'itéré 100 fois la même fonction sur les mêmes arguments dans la fonction de benchmark est d'éviter un temps d'exécution de 0 dans le cas où la fonction s'exécuterai trop vite. De plus, on répète epochs=20 fois la fonction de benchmark avec des paramètres différents afin d'éviter d'obtenir des résultats biaisés par des paramètres bien particulier

![[exercice_1_courbe_contraction_matrice.png]]

La figure précédente nous montre le benchmark obtenu. Nous pouvons donc confirmer que la complexité de cette fonction de contraction d'arête est bel et bien $\mathcal{O}(n)$. En effet, nous pouvons constater une certaine linéarité dans la forme de la courbe obtenue. 

Nous pouvons également notifier que le type de graphique choisi n'aura pas de réel impacte quant au temps d'exécution de notre fonction. En effet, cela peut paraître sensé quand nous regardons le code. Notre fonction est "aveugle" et ne s'intéresse pas au type du graph ni à la disposition des valeurs dans la matrice d'adjacence. Il ne fait que répéter les mêmes actions qu'importe l'entrée, soit faire des additions de liste et remplir des cases de la matrice par des 0.

Enfin, il est important de notifier que notre fonction de benchmark effectue, avant de lancer la fonction de contraction, une copie de l'objet MatriceAdj afin d'éviter d'appliquer la fonction sur une matrice déjà impactée.
Ainsi, le temps d'exécution de notre fonction de benchmark prend en compte la fonction + la copie de notre matrice d'adjacence. Or, on peut supposer que cette copie aura un impact sur les valeurs finales de temps (axe des ordonnées) mais non sur la forme de notre courbe.

Ainsi, on en déduit selon notre benchmark que la fonction de contraction d'arêtes est exécutable en un temps ne dépendant que de la taille de notre matrice d'adjacences (nombre de sommets dans le graphique) et étant linéaire selon cette variable.

Maintenant que nous avons réussi à avoir une fonction de contraction des arêtes, nous voulons développer l'algorithme de Karger présenté au début de l'exercice. Pour cela nous devons coder 2 nouvelles fonctions. La première fonction, propre à l'objet utilisé, sert à déterminer une valeur d'un sommet i et j afin d'effectuer la contraction sur ces deux sommet. Cette fonction fera appel à l'aléatoire suivant une loi uniforme. C'est à dire qu'on peut supposer que la probabilité de tirer une arête $x$ liant le sommet i au sommet j est :
$$
P(x) = \frac{1}{\sum_{i=1}^n\sum_{j=1}^n M_{i,j}}
$$
avec $M_{i,j}$ une valeur de la matrice d'adjacences pour le sommet i et j (soit le nombre d'arêtes entre i et j) et n le nombre total de sommets dans le graphique.

Dans le cas d'une matrice on peut reprendre cette formule afin d'obtenir, dans un premier temps, le sommet i, puis le sommet j selon la valeur de i. Ainsi on obtient les formules suivantes:
$$
P(i) = \frac{\sum_{j=1}^n M_{i,j}}{\sum_{i=1}^n\sum_{j=1}^n M_{i,j}}
$$

La probabilité d'obtenir le sommet i est égale à la somme des arètes reliées à i (pour tous j) divisée par le nombre d'arêtes au total dans le graphe.
$$
P(j|i) = \frac{M_{i,j}}{\sum_{j=1}^n M_{i,j}}
$$
La probabilité d'obtenir le sommet j en sachant que nous avons déjà obtenu j est égale à la division du nombre d'arête entre le sommet i et j divisé par la somme de toutes les arêtes relié à i.

C'est à l'aide de ces formules que nous avons pu écrire le code permettant de tirer aléatoirement un sommet i et j $\in [\![1,n]\!]$. Ce code étant propre à la structure de notre objet, nous avons choisi d'en faire une méthode de la classe MatriceAdj. 

```python
def tirage_sommet_alea(self):
	sum_aretes = self.sum_arete_sommets()
	sum_all = self.sum_all()
	i = choices(list(range(1, self.taille + 1)), [elem / sum_all for elem in sum_aretes])[0]
	j = choices(list(range(1, len(self[i - 1]) + 1)), [elem / sum(self[i - 1]) for elem in 
		self[i - 1]])[0]

	return i, j
```

Dans un second temps, après avoir codé toutes les fonctions nécessaires au bon fonctionnement de l'algorithme nous avons retranscrit celui-ci en python dans le fichier propre aux algorithmes de l'exercice 1 `./projet/exercice_1/algo.py`. 

Pour ce faire, nous avons choisi d'utiliser une dictionnaire d permettant de garder en mémoire les sommets qui sont fusionné entre eux. Ce dictionnaire prend comme clé de référence le numéro d'un sommet (pour tous les sommets) et est associé à une liste regroupant le nom de chaque sommets fusionnés avec le sommet en clé de référence. On en déduit donc que si nous avons un sommet $1$ : $[1, 2, 3]$  et un sommet $4$ : $[4,5,6]$ alors si nous fusionnons les sommets 1 et 4 on obtiendra la liste $[1,2,3,4,5,6]$ associée au sommet 1 et aucune liste associée au sommet 4. 

A la fin de l'algorithme il ne nous restera que 2 clés de référence dans notre dictionnaire et, selon ce même algorithme, nous devons retourner une valeur associée à une des deux clés.

Ainsi, nous obtenons l'algorithme suivant retranscrit en python :
```python
def algo_karger(mat: MatriceAdj | DictAjd):
	d = {i: [i] for i in range(1, len(mat) + 1)}
	while len(mat) > 2:
		i, j = mat.tirage_sommet_alea()
		d[i] += d[j]
		del d[j]
		mat.contraction(i, j)
	return list(d.values())[0]
```

Maintenant que nous avons réussi à avoir une fonction algo_karger fonctionnnelle (tester à l'aide d'un test dans le fichier `./tests/exercice_1_algo.py`) nous pouvons réaliser une étude expérimentale sur les mêmes familles de graphiques que précédemment. Cette analyse expériementale a pour but d'étudier la complexité temporelle de notre algorithme sachant qu'en théorie cette complexité est exprimé par $O(n^{2})$ pour une matrice d'adjacences.

Ainsi, après avoir écrit le fichier de benchmark correspondant `./benchmark/temporel/exercice_1_algo_matrice.py` nous obtenons les résultats suivants :

![[exercice_1_algo_matrice.png]]
Ces resultats ont été obtenus à l'aide des paramètres suivants :
- une seule itération avec le même graphique dans la fonction de benchmark
- 20 itérations de la fonction de benchmark (soit 20 de l'algorithme pour chaque taille n)
- une taille de graphique n variant de 5 à 100 avec un pas de 5

On remarque ici que nous obtenons bien des courbes non linéaires correspondant effectivement à une complexité temporelle de $O(n^{2})$. De plus, on remarque que le temps dexécution de l'algorithmes n'a pas l'air de dépendre du graphique en entrée mais plutôt de sa taille. En effet, les valeurs de y (axe des ordonnées) associées à x (axe des abscisses) n'ont pas l'air de varier de manière significative en fonction du graphique. On en déduit donc que la taille du graphique est la seule variable de notre fonction de complexité temporelle.


## Structure de type Dictionnaire

Nous souhaitons maintenant déterminer une deuxième structure de données utilisable pour notre algorithme de Karger. Ainsi, il faut que cette structure de données (nouvelle classe d'objet) soit capable de :
- stocker les liaisons entre un sommet i et un sommet j
- déterminer un sommet i et j aléatoirement suivant une loi uniforme 
- effectuer une contraction de deux sommets i et j

Pour cela nous avons choisi de prendre une structure de données totalement différentes des matrices afin de bien observer l'impacte des structures de données. Ainsi nous avons choisi de réaliser un dictionnaire avec une clé de référence indiquant un sommet et une liste associée indiquant les sommets liés à celui-ci. Dans l'idée cela peut ressembler au dictionnaire mis en place dans l'algorithme de Karger montré précédemment. 

Cette structure de données est bien différente des matrices car les listes ne fonctionnent pas de la même façon que les tables de hashage. En effet, dans une matrice nous souhaitons indiqué le nombre d'arêtes entre le sommet i et j alors que dans notre table de hashage si deux arêtes sont présent entre le sommet i et j alors nous aurons, pour la clé de référence i, deux fois la valeur de j dans la liste associée.

Cependant, cette différence de structure ne vient pas modifier le propriété de symétrie présent dans la matrice d'adjacence. Ici cette symétrie se présente par l'idée de répéter l'information de liaison des sommets i et j pour chaque clé de référence i et j.

Ainsi, nous pouvons dans un premier temps réécrire une fonction de contraction pour cette nouvelle structure. Le principe de contraction est le même cependant on l'adaptera aux variables de type dictionnaire. C'est pourquoi, à la place de faire une addition de deux listes (cas utiliser pour les matrices d'adjacences), nous allons faire une fusion de deux listes. En effet, lorsqu'un sommet i et j seront fusionnés alors la liste associée à la clé de référence j sera fusionné dans la liste associée à la clé de référence i. En revanche, lors de cette fusion il faudra faire attention à ne pas prendre les éléments i et j car i ne peut pas être lié à lui même et j n'existera plus à la fin de la contraction. Ensuite, il nous reste à changer les valeurs de j par i pour chaque clé de référence présente dans la liste associée à la clé de référence i (application de la symétrie). Enfin, contrairement aux matrices d'adjacences, nous pouvons supprimer la clé de référence j pour indiquer que le sommet a bien été supprimé de notre graphique.

Une fois retranscrit en python, nous obtenons le code suivant :
```python 
def contraction(self, i, j):
	# Ajout des valeurs de j dans i
	self.dico[i] = [elem for elem in self.dico[j] + self.dico[i] if elem != i and elem != j]
	# Remplacement de j par i dans tous les k appartenant au nouveau i
	for k in self.dico[i]:
		self.dico[k] = [elem if elem != j else i for elem in self.dico[k]]
	# Suppression de j
	del self.dico[j]
	# Return du dico si nécessaire
	return self.dico
```

Le test de cette méthode a pu être validé à l'aide du test présent dans le fichier `./tests/exercice_1_dict.py`

Maintenant que nous avons une fonction de contraction, il ne nous reste qu'à déterminer une fonction de tirage aléatoire pour i et j afin de pouvoir utiliser l'algorithme de Karger sur notre structure de données. Cette fonction est très similaire à celle utilisée pour la structure précédente. En effet, les formules restent les mêmes, seules les variables changent : le nombre totale d'arêtes liées à un sommet j n'est plus calculé par la somme de la ligne i dans la matrice d'adjacences mais par la longueur de la liste associée à la clé de référence i. 

Ainsi nous obtenons le code suivant (validé par un test écrit dans le même fichier que pour contraction)

```python 
def tirage_sommet_alea(self):
	sum_aretes = self.sum_arete_sommets()
	sum_all = self.sum_all()
	i = choices(list(self.dico.keys()), [elem / sum_all for elem in sum_aretes])[0]
	j = choices(self[i], [elem / sum(self[i]) for elem in self[i]])[0]
	return i, j
```


Maintenant que nous avons une nouvelle structure de données utilisable avec l'algorithme de Karger nous pouvons étudier la différence de complexité obtenue entre les deux structure de données. Nous pouvons nous attendre à ce que la complexité de l'algorithme avec ces deux structures soit approxivement similaire, soit une courbe de la forme $\mathcal{O}(n_{2})$. En revanche, nous pouvons aussi nous attendre que le temps d'exécution de l'algorithme utilisant une table de hashage prendra plus de temps du fait que ce type est plus lent dans sa fonctionnalité que de simples listes doubles (matrices). 


![[exercice_1_courbe_algo_dict.png]]

Ces resultats ont été obtenues à l'aide des paramètres suivants :
- une seule itération avec le même graphique dans la fonction de benchmark
- 20 itérations de la fonction de benchmark (soit 20 de l'algorithme pour chaque taille n)
- une taille de graphique n variant de 5 à 100 avec un pas de 5

Le code pour obtenir ce benchmark est éditable depuis `./benchmark/temporel/exercice_1_algo_dict.py`

On remarque effectivement que l'algortithme appliqué à ce type de structure de données obtient une courbe de complexité temporelle relativement similaire à celle obtenue avec la structure précédente. En revanche, il est important de remarquer également que le temps d'exécution obtenue varie en fonction de la taille du graphique mais aussi de son type. Par exemple, un graphique de type cycle (peu de liaison) prendra beaucoup moins de temps qu'un graph complet (beaucoup de liaisons). 

On peut donc en déduire qu'avec ce type de structure, notre algorithme de Karger n'est pas aveugle quant au type de graphique passé en entrée. Cela peut s'expliquer par le fait que notre structure utilisant les matrices d'adjacences ne change jamais de taille contrairement à notre structure utilisant des dictionnaires. En effet, ce type de structure change constamment de taille puisque nous supprimons des clés et faisons des fusions de listes entre elles afin de les stocker dans d'anciennes clés de références. Comme indiqué précédemment, on peut donc se douter que l'algorithme de Karger utilisant un graphique ayant peu de liaisons ira plus vite que celui utilisant un graphique avec beaucoup de liaisons. En effet, par comparaison avec une matrice d'adjacence, un dictionnaire aura autant de clés de référence que de ligne dans la matrice mais quasiment jamais autant de valeurs dans les listes associées que de colonnes dans une matrice. Ainsi, la rapidité de notre algorithme ne dépend que de la taille des listes associées à des clés de référence dans notre dictionnaire de données.

Par ailleurs, on remarque également que pour un graphique en cycle, il est préférable d'utiliser ce type de structures.

On peut donc en conclure de cet exercice que l'algorithme de Karger permet d'obtenir des résultats relativement rapidement pour un problème de coupe minimale dans un graphique. Cependant le type de structures de données sera important quant à la vitesse d'exécution de notre algorithme. Certaines structures de données feront que notre algorithme sera "aveugle" quant au type de graphique passé en entrée, seule sa taille importera afin de connaître le temps d'exécution de l'algorithme. En revanche, il nous est également possible de prendre une structure de données faisant l'inverse, le temps d'exécution de notre algorithme dépendra du graphique en entrée. Dans ce cas l'algorithme pourra prendre plus ou moins de temps pour effectuer la même tâche que précédemment. Il est donc judicieux de faire attention au type de structure utilisé afin de d'avoir un temps d'exécution optimale pour chacun de nos graphiques.

<div style="page-break-after: always;"></div>


# Exercice 2

## Taux de réussite de notre algorithme

Maintenant que nous avons réussi à comparer la complexité temporelle de l'algorithme de Karger en fonction de la structure de données passée en entrée, nous souhaitons comparer la complexité temporelle et le taux de réussite de cet algorithmes avec d'autres algorithmes (amélioration de l'algorithme de Karger).

Dans un premier temps nous souhaitons nous intéresser au taux de réussite de notre algorithme de Karger en fonction des graphiques et de leur taille. Ici nous ne nous intéressons pas aux différences entre les structures de données car leur fonctionnement restent le même. Ainsi nous utiliserons des benchmarks de probabilité qui représentent la capacité d'un algorithme à trouver une réponse optimale pour un graphique en entrée en fonction de sa taille.

Dans ce benchmark nous ne souhaitons plus utiliser les graphiques aléatoires car il nous est impossible de les générer aléatoirement et de connaître en même temps le cardinal d'une coupe minimale du graph. Ici, nous n'utiliserons que des structures de données de type Matrice d'adjacence afin de limiter le temps d'exécution de nos programmes.

![[exercice_2_courbe_reussite_algo_karger.png]]

Le graphique ci dessous a été réalisé à l'aide des paramètre suivants :
- Pour chaque n taille on vérifie la probabilité de réussite de l'algorithme pour 1000 échantillons aléatoires
- La taille n varie de 5 à 45 avec un pas de 5

On peut remarquer sur la dernière figure deux points importants. Le premier est que nous avons une probabilité de réussite de l'algorithme qui augmente lorsque la taille de notre graphique augmente pour les graphiques complets et les graphiques bipartis complets, cela est logique et ressemble à la théorique. Le second point est que nous obtenons une probabilité constante de 100% de réussite pour le graphique en cycle. Après vérification, nous pouvons en déduire que cela est normal. En effet, imaginons un graphique en cycle comme un rond ayant une infinité de sommet. Dans le cas où nous avons un coupure nette, telle une droite, passant d'un côté à l'autre de ce cercle, cette droite ne pourra passer que par deux arêtes. Ainsi, qu'importe le nombre de sommets ou bien l'endroit de la coupure, il ne sera pas possible d'obtenir autre chose qu'un cardinal d'une coupe égale à 2.


## Algorithme de Karger itéré

Maintenant que nous avons réussi à obtenir obtenir un algorithme de Karger fonctionnelle nous souhaitons comparer ses résultats avec un algorithme de Karger itéré. Cet algorithme est une amélioration de l'algorithme de Karger puisqu'il reprend son fonctionnement. Cependant ici, nous itérerons T fois l'algorithme de Karger afin d'obtenir le meilleur résultat possible. Ainsi nous avons l'algorithme suivant : 

![[Algo_Karger_2.png]]

Dans cet exercice, nous souhaitons comparer le temps d'exécution de nos deux algorithmes (Karger et Karger itéré). Pour cela nous allons continuer d'utiliser la structure de la matrice d'adjacences afin de minimiser le temps d'exécution des algorithmes. 

Ainsi, dans un premier temps nous devons retranscrire en python l'algorithme de Karger itéré. Cela n'est pas difficile, il suffit de reprendre l'algorithme de Karger déjà écrit et d'écrire les conditions et boucles présentées dans l'énoncé précédent.

```python 
def algo_karger_itere(mat: MatriceAdj | DictAjd, T: int):
	m_opti = inf
	s_opti = []
	for _ in range(T):
		mat_copy = mat.copy()
		s = algo_karger(mat_copy)
		m = cardinal(mat, s, mat.sommets)
		if m < m_opti:
			m_opti = m
			s_opti = s
	return s_opti
```

Il est important de préciser qu'il faut effectuer une copie de la structure représentant le graphique à chaque itération de T afin de ne pas modifier cet algorithme la matrice au fur et à mesure de l'itération. Cela risque de fausser les résultats. 

Maintenant que nous avons nos deux algorithmes, nous souhaitons voir la diférence de ceux-ci sur le plan de la complexité temporelle et de leur capacité à obenir un résultat optimal. Pour cela, dans un premier temps, nous analiserons la complexité temporelle de notre algorithme de Karger itéré à l'aide d'une étude expérimentale (voir fichier `./benchmark/temporel/exercice_2_algo.py`)

Il est évident, à la vu de notre algorithme, qu'il sera plus long à exécuter que l'algorithme de Karger car celui-ci sera répété T fois (choix arbitraire) afin d'obtenir un résultat optimale. On peut donc s'attendre à ce que le temps d'exécution de l'algorithme de Karger itéré soit T fois plus long que l'algorithme de Karger simple.

![[exercice_2_courbe_algo_karget_itere.png]]

Ces figures ont été obtenu à l'aide des paramètres suivants :
- T = 5
- La taille n des graphiques varient de 5 à 100 avec un pas de 5
- Aucune itération dans la fonction de benchmark
- 20 itération de la fonction de benchmark par valeur n

Comme définit dans les paramètres précédents, il est important que les paramètres de ce benchmark soient simialires à ceux pour l'algorithme de Karger simple. En effet, si nous n'avons pas les mêmes paramètres cela entraînera des erreurs de comparaison. Ainsi, la seule valeur changeante est la valeur de T puisqu'il s'agit d'un nouveau paramètre.

Nous pouvons effectivement voir ici que les courbes de notre algorithme gardent la même allure. En revanche, la valeur de T vient bel et bien impacté le temps d'exécution de notre algorithme de Karger itéré. En effet, on remarque que le temps d'exécution de notre algorithme est presque T fois plus long avec cette algorithme itéré qu'avec l'algorithme de Karger simple. 

De plus, comme on pouvait s'en douter, le temps d'exécution des algorithmes n'a pas l'air de varier fortement entre deux graphiques différents. La petites différence que l'on voit est dû à la légère différence à peine visibile pour l'algorithme de Karger simple qui a été multiplié par T = 5.

On peut donc affirmer que le temps d'exécution de l'algorithme de Karger itéré est plus long que celui de l'algorithme de Karger simple. En revanche, on peut s'attendre à ce que sa capacité à obtenir un résultat optimale soit plus grande. En effet, sachant qu'on garde le meilleur résultat de l'algorithme de Karger sur T itérations il est logique que plus T est grand, plus il est possible de tomber sur une valeur optimale du problème pour un graphique donné.  

Ainsi nous avons le benchmark de probabilité suivant qui montre le taux de réussite de l'algorithme de Karger itéré en fonction de la taille des graphiques:
![[exercice_2_courbe_reussite_algo_karger_itere.png]]

Ce benchmark a été réalisé avec les paramètres suivants :
- T = 5
- La taille n d'un graphique varie entre 5 et 45 avec un pas de 5
- Pour chaque n, le taux de réussite a été obtenu sur un échantillon de 1000 graphiques

Comme nous pouvons remarquer ce benchmark de probabilité nous montre que nous arivons à avoir une solution optimale plus souvent à l'aide de ce nouvel algorithme. Un point important à remarquer est que la solution optimale sera quasiment trouver dans 100% des cas. Par logique, on peut en déduire que plus T est grand, plus la solution trouvée aura des chances d'être optimale.

<div style="page-break-after: always;"></div>

# Exercice 3

Maintenant que nous avons vérifié ce qu'il se passait si nous utilisons un algorithme de Karger avec des itérations, nous souhaitons nous intéresser à la différence de cet algorithme avec celui de Karger-Stein. 
Cet algorithme est différent de celui vu précédemment. Ici, nous souhaitons faire des contraction partielles indépendantes du graphe jusqu'à ce qu'il reste environ $\frac{n}{\sqrt{ 2 }}$ sommets. Il nous suffit par la suite de chercher récursivement une coupe minimale dans le multigraphe obtenus. 

![Algo Karger](img/contraction_partiel.png)

![Algo Karger-Stein](img/Algo_Karger_Stein.png)

## Analyse théorique

Dans un premier temps nous souhaitons faire une étude théorique de cet algorithme. C'est pourquoi nous cherchons la complexité temporelle de l'algorithme de Karger-Stein.

Soit $T(n)$ le temps d'exécution de l'algo pour un graphe à $n$ sommets.
Pour un graphe de taille $n > 6$, on réalise deux contractions partielles ce qui nous donne deux graphes à $t = \left\lceil 1+ \frac{n}{\sqrt 2} \right\rceil$ sommets de temps d'exécution $T(t)$.
De plus, la contraction partiel ce fait en $O(n^2)$, car une contraction se fait en $O(n)$ et on fait $\left\lceil 1+ \frac{n}{\sqrt 2} \right\rceil$ contraction.
Donc on obtient,
$$T(n)=2T\left(\left\lceil 1+ \frac{n}{\sqrt 2} \right\rceil\right)+O(n^2)$$
Comme $\lim\limits_{n \to \infty} \frac{\lceil 1+ n/\sqrt 2 \rceil}{n/\sqrt 2} = 1$, 
$$T(n) = aT\left(\frac{n}{b}\right)+O(n^2) \text{ avec } a = 2, b = \sqrt 2 \text{ et } k = 2$$ 
Or, $a = b^k$ donc d'après le théorème maître:
$$T(n) = O(n^2 \log n)$$

Maintenant que l'on connait la complexité de l'algorithme, on cherche sa probabilité de réussite.
Pour $n>6$:
$$
P(n) = 1- P(\bar n) \text{ avec } P(\bar n) = P(\bar t)^2 \\
P(n) = 1- P(\bar t)^2 \text{ avec } P(\bar t) = 1 - P(t) \\
\boxed{P(n) = 1- (1 - P(t))^2}
$$

En partant de $P(t) \ge \frac{1}{2}P(t)$
$$
\begin{align*}
P(t) &\ge \frac{1}{2}P(t) \\
1-P(t) &\le 1-\frac{1}{2}P(t) \\
(1-P(t))^2 &\le (1-\frac{1}{2}P(t))^2 \\
1-(1-P(t))^2 &\ge 1-(1-\frac{1}{2}P(t))^2
\end{align*} \\
\boxed{P(n) \ge 1-\left(1-\frac{1}{2}P\left( 1+ \frac{n}{\sqrt 2} \right)\right)^2} 
$$

## Analyse pratique

Dans un premier temps, afin de mettre en place cet algorithme, nous allons nous intéresser à l'algorithme de contraction partielle.  Cet algorithme permet de retourner un multi-graphe à t sommets à partir d'un autre graphe non orienté.

Ainsi en traduisant l'algorithme 3 de l'énoncé on obtient le code suivant : 
```python
def contraction_partielle(mat: MatriceAdj | DictAjd, t: int) -> MatriceAdj | DictAjd:
    g = mat.copy()
    while len(g) > t:
        i, j = g.tirage_sommet_alea()
        g.contraction(i, j)
    return g
```

Tout comme précédemment, il est important d'effectuer une copie de la structure représentant le graphique en entrée pour éviter de modifier celui-ci et d'impacter le résultat de la solution. Ainsi, tant que le nombre de sommets dans g est inférieur au paramètre t, il faut piocher une arête aléatoire et faire la contraction des sommets dans le graph. On retourne le multi-graphe que nous venons de construire.

Maintenant que nous avons notre fonction de contraction partielle, il est possible de retranscrire en python l'algorithme de Karger-Stein qui est une fonction récursive

```python
def algo_karger_stein(mat: MatriceAdj | DictAjd):
	if len(mat) <= 6:
		return coupe_min(mat)
	
	t = ceil(1 + len(mat) / sqrt(2))
	v = mat.sommets
	
	g1 = contraction_partielle(mat, t)
	s1 = algo_karger_stein(g1)
	m1 = cardinal(mat, s1, v)
	
	g2 = contraction_partielle(mat, t)
	s2 = algo_karger_stein(g2)
	m2 = cardinal(mat, s2, v)
	
	if m1 < m2:
		return s1
	return s2
```

On remarque ici que la seule fonction qu'il nous reste à codé est la coupe minimum d'une matrice pour toutes les possibilité possibles. Cette fonctionnalité est réalisé par la fonction coupe_min suivante : 
```python 
def coupe_min(mat: MatriceAdj | DictAjd):
	v = mat.sommets
	cmin = inf
	smin = None
	for s in chain.from_iterable(combinations(v, r) for r in range(1, len(mat))):
		if (c := cardinal(mat, s, v)) < cmin:
			cmin = c
			smin = s
	return smin
```

Maintenant que nous avons réussi à retranscrire le nouvel algorithme, nous souhaitons voir si celui-ci est plus intéressant que l'algorithme de Karger ou que l'algorithme de Karger itéré par rapport à son temps d'exécution ainsi qu'à son résultat.

Dans un premier temps nous allons effectuer un benchmark de son temps d'exécution. Pour cela nous allons utiliser le fichier de benchmark temporelle `./benchmark/temporel/exercice_3_algo_karger_stein.py`. 
![[exercice_3_courbe_algo_karger_stein.png]]


Les graphiques ci-dessus ont été obtenues à l'aide des paramètres de benchmark suivant : 
- La fonction de benchmark n'exécute qu'une seule fois la fonction que nous souhaitons observer
- Pour chaque taille n, on relance la fonction de benchmark sur un nouveau graphique 20 fois 
- La taille n du graphique varie de 5 à 45 par pas de 5

Ces paramètres sont quasiment les mêmes que ceux utilisés dans les benchmark temporelles des exercices précédents. En effet, par soucis de temps d'exécution nous ne pouvions pas exécuter notre algorithme sur des tailles de graphes allant de 5 à 100 par pas de 5. Tout comme les exercices précédents, nous continuons de générer des graphes aléatoires pour chaque valeur de epochs. En effet, cela permet de garantir que nous ne biaisons pas nos résultats.

Comme nous pouvons le remarquer notre algorithme de Karger Stein prend plus de temps que tous les autres algorithmes réalisés dans les exercices précédents. Cependant, au-delà de cette information qui pouvait sembler logique, un autre point important est présent sur ces courbes. Dans un premier temps toutes les courbes ont la même forme et montre que l'algorithme ne dépend pas du type de graphe en utilisant une structure de données et type Matrice d'adjacences. En revanche, on remarque certains sursauts et coupures qui sur nos courbes. Cela pourrait paraître aléatoire si une seule courbe présentait ce type d'informations, or ici toutes nos courbes agissent de la même façon à des endroits similaires. On peut donc en déduire qu'il existe sûrement un facteur dans notre nouvel algorithme qui influent le temps d'exécution de notre algorithme qui n'existait pas précédemment.

Maintenant que nous savons que cet algorithme s'exécute moins rapidement que tous nos autres algorithmes, on souhaite savoir si une potentielle amélioration des résultats peut venir justifier ce temps d'exécution plus long. 
Pour cela, nous souhaitons lancer un benchmark de probabilité de réussite de notre algorithme selon les mêmes paramètres que les benchmark de probabilité précédent (voir fichier `./benchmark/proba/exercice_3_algo_karger_stein.py`)

![[exercice_3_courbe_reussite_algo_karger_stein.png]]

On remarque sur la figure précédente que nous arrivons à avoir un taux de réussite de 100% pour chacun de nos graphes. On peut alors se douter que l'algorithme de Karger-Stein est un algorithme prenant plus de temps que tous les autres algorithmes mais qui permet d'avoir dans plus de 99% des cas une solution optimale au problème de coupe minimale.

Tout comme l'algorithme de Karger, nous souhaitons désormais nous intéresser au cas de l'algorithme de Karger Stein itéré. Nous pouvons nous douter que celui-ci ne pourrai pas avoir une meilleure solution optimale puisque l'algorithme de Karger Stein arrive à avoir 100% de bonnes réponses dans nos tests. En revanche, on peut se demander si l'algorithme de Karger Stein itéré (qui peu potentiellement être sûr de trouver une bonne réponse dans le cas où l'algorithme de Karger Stein venait à échouer) a la même courbe d'évolution du temps d'exécution.

Tout d'abord l'algorithme de Karger-Stein itéré est développé à l'aide de la fonction suivante. Afin de développer cette fonction, nous nous sommes inspiré de la fonction Karger itéré.

```python
def algo_karger_stein_itere(mat: MatriceAdj | DictAjd, T: int):
	m_opti = inf
	s_opti = []
	for _ in range(T):
		mat_copy = mat.copy()
		s = algo_karger_stein(mat_copy)
	
		if m < m_opti:
			m_opti = m
			s_opti = s
	return s_opti
```


Ainsi, à l'aide de notre dernier fichier de benchmark temporel `./benchmark/temporel/exercice_3_algo_karger_stein_itere.py`. On peut se douter, après avoir étudié les courbes de l'algorithme de Karger itéré, que l'algotihme de Karger Stein itéré aura un temps d'exécution T fois plus long.


![[exercice_3_courbe_algo_karger_stein_itere.png]]

Ainsi, on constate deux points sur ces courbes. Le premier est qu'effectivement le temps d'exécution est T fois plus long pour l'algorithme de Karger Stein itéré que pour l'algorithme de Karger Stein. Le deuxième point est que nous retrouvons de nouveau cette cassure de la courbe qui vient nous indiquer, une nouvelle fois, qu'une fonctionnalité de notre algorithme vient bien avoir un impact sur la continuité de la courbe d'évolution du temps d'exécution de notre algorithme.

<div style="page-break-after: always;"></div>

# Conclusion

Après avoir étudié, lors de ce projet, plusieurs algorithmes, nous avons pu constater que plusieurs algorithmes étaient possibles pour résoudre un problème précis. En revanche, le taux de réussite et le temps d'exécution peuvent varier d'un algorithme à un autre. Ainsi, il est important de trouver un bon compromis entre ces deux facteurs pour trouver un algorithme qui sera le plus intéressant dans une certaine situation.

Nous avons également pu constaté que la structure des données peut avoir un impact significatif sur le temps d'exécution d'un algorithme. Certaines structure de données sont plus adapté à certaines situations. Cependant la structure de données n'aura pas d'impact sur le résultat finale de l'algorithme car son fonctionnement restera toujours le même.

Enfin, nous avons pu prouver que le résultat d'un algorithme utilisant l'aléatoire n'était pas forcément mauvais et aléatoire. En effet, chacun de nos algorithmes utilisaient une fonction de choix aléatoire pour déterminer une arête. De plus, ce choix aléatoire permet de ne pas biaisé les résultats en sortie de notre algorithme. Cependant nous avons pu constater que certaines fois, les résultats en sorti de nos algorithmes n'étaient pas forcément des résultats possibles dû à l'aléatoire (cas rare).